
    <?php
$h1 = "Contatos Elétricos de Cobre";
$title  =  $h1;
$cliente_minisite = "Barbanera";
$minisite = "barbanera";
$desc = "Os Contatos Elétricos de Cobre garantem alta condutividade, resistência e eficiência na transmissão elétrica. Solicite uma cotação no Soluções Industriais!";
include "inc/$minisite/inc/head.php";
?>
</head>

<body>
    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>

    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
                <?php include "$prefix_includes" . "inc/product-images.php" ?>
                <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
            </section>
                <?php include "$prefix_includes" . "inc/product-aside.php" ?>
        </section>
        
        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>
    