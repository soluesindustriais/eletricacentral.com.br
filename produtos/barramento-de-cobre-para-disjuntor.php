
    <?php
$h1 = "Barramento de Cobre para Disjuntor";
$title  =  $h1;
$cliente_minisite = "Barbanera";
$minisite = "barbanera";
$desc = "O Barramento de Cobre para Disjuntor garante condução eficiente e segura de corrente elétrica em painéis e quadros de distribuição. Solicite uma cotação!";
include "inc/$minisite/inc/head.php";
?>
</head>

<body>
    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>

    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
                <?php include "$prefix_includes" . "inc/product-images.php" ?>
                <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
            </section>
                <?php include "$prefix_includes" . "inc/product-aside.php" ?>
        </section>
        
        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>
    