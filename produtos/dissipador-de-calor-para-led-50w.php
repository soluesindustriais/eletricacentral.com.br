<?php
$h1 = "Dissipador de Calor para LED 50W";
$title  =  $h1;
$cliente_minisite = "Usinagem JK";
$minisite = "usinagemjk";
$desc = "Dissipador de Calor para LED 50W garante eficiência térmica e prolonga a vida útil das luminárias. Ideal para aplicações industriais e comerciais.";
include "inc/$minisite/inc/head.php";
?>
</head>

<body>
    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>

    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
                <?php include "$prefix_includes" . "inc/product-images.php" ?>
                <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
            </section>
            <?php include "$prefix_includes" . "inc/product-aside.php" ?>
        </section>

        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>