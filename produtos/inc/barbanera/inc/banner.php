<!-- #Alterar banner -->
<!-- Aqui você deve alterar os títulos e mensagens do banner e a imagem do background, e o link dos cliques -->
	<?php if (!$isMobile) : ?>
		<div class="slick-banner">
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.5) 50%, rgba(36, 36, 36, 0.5) 50%), url('<?= $url ?>imagens/banner/banner1.webp')">
				<div class="content-banner">
					<h1>BARBANERA</h1>
					<p>Inovação e Tecnologia para Impulsionar Sua Produção!</p>
					<a class="btn-banner" href="<?= $url ?>sobre-nos" title="Página sobre nós">Saiba Mais</a>
				</div>
			</div>
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.7) 50%, rgba(36, 36, 36, 0.7) 50%), url('<?= $url ?>imagens/banner/banner2.webp')">
				<div>
					<div class="content-banner justify-content-evenly row">
						<div>
							<h2>A Energia Que Move Sua Indústria!</h2>
							<p class="text-banner">A solução elétrica que você precisa, com a qualidade que você confia!</p>
						<a class="btn-banner" href="<?= $url ?>catalogo" title="Catalogo de Produtos">Conheça Nossos Produtos</a>
						</div>
						<!-- <div>
							<img src="<?= $url ?>imagens/clientes/logo-categoria.png" alt="Logo da categoria" title="logo do cliente" class="slick-thumb">
						</div> -->
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>