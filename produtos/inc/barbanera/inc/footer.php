<footer>
	<div class="wrapper">
		<div class="contact-footer">
			<address>
				<span><?php echo $nomeSite . " - " . $slogan ?></span>
				<p><?php echo $rua . " - " . $bairro ?></p> 
				<p><?php echo $cidade . " - " . $UF . " - " . $cep ?></p> 
				<span style="margin-top: 10px;"><?php echo "Entre em contato" ?></span>
				<p><?php echo "Email: " . $cliente_email ?></p> 
				<p><?php echo "Telefone: " . $cliente_telefone ?></p> 
			</address>
			<br>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<?php 
					foreach($menuItems as $value => $key){
						echo '<li><a href="'.$link_minisite_subdominio.$key["url"].'" title="'.$value.'">'.$value.'</a></li>';
					}
					?>
					<li><a href="<?= $link_minisite_subdominio ?>mapa-site" title="Mapa do site <?= $nomeSite ?>">Mapa do site</a></li>
				</ul>
			</nav>
		</div>
		<br class="clear">
	</div>
</footer>
<div class="copyright-footer">
	<div class="wrapper">
		Copyright © <?= $nomeSite ?>. (Lei 9610 de 19/02/1998)
		<div class="selos">
			<a rel="nofollow" href="https://validator.w3.org/nu/?showsource=yes&doc=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" target="_blank" title="HTML5 W3C"><i class="fa-brands fa-html5"></i><strong>W3C</strong></a>
			<a rel="nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=pt-BR" target="_blank" title="CSS W3C"><i class="fa-brands fa-css3"></i><strong>W3C</strong></a>
		</div>
	</div>
</div>
<?php
include "$prefix_includes" . "inc/header-fade.php";
include "$prefix_includes" . "inc/readmore.php";
include "$prefix_includes" . "inc/header-scroll.php";
if (strpos($url, "localhost") !== false) {
	if (in_array($archive, $archives_subdomain)) {
		include "$prefix_includes" . "inc/classes/create-page-minisite.php";
	}
}
?>


<?php if (isset($breadJsonEncoded)) :
	echo "<script type='application/ld+json'>" . $breadJsonEncoded . "</script>";
endif; ?>


<!-- ANALYTICS -->
<?php
foreach ($tagsanalytic as $analytics) {
	echo '<script async src="https://www.googletagmanager.com/gtag/js?id=' . $analytics . '"></script>' . '<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);}' . "gtag('js', new Date()); gtag('config', '$analytics')</script>";
}
?>
<!-- fim ANALYTICS -->