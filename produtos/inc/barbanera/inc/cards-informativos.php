<section class="wrapper card-informativo">
    <h2>Nossos principios</h2>
        <div class="cards-informativos">
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-map"></i>
            <h3>Missão</h3>
            <p>Fornecer produtos eletromecânicos de alta qualidade, com precisão e acabamento diferenciados, atendendo às necessidades da indústria com eficiência, inovação e compromisso com a satisfação dos clientes.</p>
        </div>
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-award"></i>
            <h3>Visão</h3>
        <p>Ser referência nacional e internacional no segmento eletromecânico, reconhecida pela excelência em qualidade, inovação e confiabilidade, consolidando parcerias de longo prazo com clientes e fornecedores.</p>
    </div>
    <div class="cards-informativos-infos">
        <i class="fa-solid fa-scale-balanced"></i>
        <h3>Valores</h3>
        <p>Compromisso com a qualidade e precisão, inovação e busca constante por melhorias, ética e transparência em todas as relações, valorização das pessoas e do trabalho em equipe, respeito aos prazos e responsabilidade socioambiental.</p>
    </div>
</div>
</section>
