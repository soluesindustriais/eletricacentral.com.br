<!-- Aqui você deve alterar os títulos e mensagens do banner e a imagem do background, e o link dos cliques -->
	<?php if (!$isMobile) : ?>
		<div class="slick-banner">
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.7) 50%, rgba(36, 36, 36, 0.7) 50%), url('<?= $url ?>imagens/banner/banner-1.webp')">
				<div>
					<div class="content-banner justify-content-evenly row">
						<div>
							<h1>JK Usinagem: Transformando Experiência em Excelência</h1>
							<p>De um pequeno quartinho a uma referência em qualidade e ética no mercado, construímos juntos uma história de dedicação e inovação.</p>
							<a class="btn" href="<?= $url ?>catalogo" title="Catalogo de Produtos">Clique</a>
						</div>
						<div>
							<img src="<?= $url ?>imagens/clientes/logo-categoria.png" alt="Logo da categoria" title="logo do cliente" class="slick-thumb">
						</div>
					</div>
				</div>
			</div>
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.5) 50%, rgba(36, 36, 36, 0.5) 50%), url('<?= $url ?>imagens/banner/banner-2.webp')">
				<div class="content-banner">
					<h2>Paixão e Precisão</h2>
					<p>Uma história de qualidade e confiança desde 2012.</p>
					<a class="btn" href="<?= $url ?>sobre-nos" title="Página sobre nós">Clique</a>
				</div>
			</div>
	
		</div>
	<?php endif; ?>