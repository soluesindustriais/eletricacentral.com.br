<!-- #Alterar mudar os icones, e textos e mover o style para a head -->
<style>
    .cards-informativos{
        display: flex;
        justify-content: space-around;
        margin: 0px auto;
        max-width: 1024px;
        align-items: center;
        flex-wrap: wrap;
    }
    .cards-informativos > 
    .cards-informativos-infos{
        background-color: #fff;
        box-shadow: 0px 0px 8px #31313131;
        padding: 16px;
        border-radius: 1rem;
        width: 300px;
        margin-top: 10px;
        margin: 20px 0px;
        height: 300px;
        cursor: pointer;
        transition: 0.3s ;
    }
    .cards-informativos-infos p{
        text-align: left;
    }
    .cards-informativos-infos i{
        color: var(--color-secundary);
        font-size: 1.8rem;
    }
    .cards-informativos-infos h3{
        text-align: center;
    }
    .cards-informativos-infos:hover{
        margin-top: 0px;
        transition: 0.3s ;
    }
    .card-informativo h2, .card-informativo span p{
        text-align: center;
        font-weight: bold;
    }
    .card-informativo span p{
        font-size: 1.1rem;
        margin: 0px;
    }
    .card-informativo h2{
        margin: 0px;
        font-size: 2rem;
        color: var(--color-secundary);
    }
</style>
<section class="wrapper card-informativo">
    <span><p>O que nós fornecemos</p></span>
    <h2>Nossos principais focos</h2>
        <div class="cards-informativos">
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-map"></i>
            <h3>Missão</h3>
            <p>Oferecer soluções em usinagem com excelência, garantindo a máxima qualidade e um atendimento diferenciado. Trabalhar com dedicação e inovação para superar as expectativas de nossos clientes, contribuindo para o crescimento do setor e da comunidade.</p>
        </div>
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-map"></i>
            <h3>Visão</h3>
        <p>Ser referência no mercado de usinagem, reconhecida por nossa qualidade, ética e compromisso com a satisfação dos clientes. Buscar constantemente o aprimoramento, a inovação e o crescimento sustentável, impactando positivamente a sociedade.</p>
    </div>
    <div class="cards-informativos-infos">
        <i class="fa-solid fa-map"></i>
        <h3>Valores</h3>
        <p>Na JK Usinagem, valorizamos a qualidade em cada produto e serviço, buscando atender aos mais altos padrões. Atuamos com ética, transparência e responsabilidade em todas as nossas relações.</p>
    </div>
</div>
</section>
