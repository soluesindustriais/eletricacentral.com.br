<?php
// Obter o nome do diretório raiz dinamicamente, assumindo que o script está na raiz de `blog`
$directoryPath = dirname($_SERVER['SCRIPT_FILENAME']);
echo $directoryPath;
// Configurações iniciais com caminhos dinâmicos
$clientConfigPath = $directoryPath . '/doutor/_app/Config/Client.inc.php';  // Ajuste para o caminho correto

// Gerar token e adicionar ao Client.inc.php
$token = bin2hex(random_bytes(8));  // Gera um token de 16 caracteres
file_put_contents($clientConfigPath, "define('TOKEN_API', '$token');\n", FILE_APPEND);
echo "Token adicionado ao arquivo Client.inc.php.<br>";
?>
