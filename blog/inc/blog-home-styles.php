<?php
    $catHomeId = "1";

    $Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :st AND cat_id = :catid", "st=2&catid=".$catHomeId);
    $homeCategory = $Read->getResult()[0];

    if(!$homeCategory):
        $Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :st", "st=2");
        $homeCategory = $Read->getResult()[0];
    endif;

    if ($homeCategory):
        $catHomeTheme = $homeCategory['cat_home_theme'];
        $catHomeId = $homeCategory['cat_id'];
        $catHomeParent = $homeCategory['cat_parent'];

        switch($catHomeTheme){
            case "Grid": include('css/blog-grid.css');
                break;
            case "List": include('css/blog-list.css');
                break;
            case "Full": include('css/blog-full.css');
                break;
            default:
                break;
        }   
    endif;
?>