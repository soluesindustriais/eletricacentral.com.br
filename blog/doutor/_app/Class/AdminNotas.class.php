<?php
/**
* AdminNotas [ CLASSE ]
* Classe resposável por gerir as notas, mensagens da empresa
* @copyright (c) 2016, Rafael da Silva Lima - Doutores da Web
*/
class AdminNotas {
//Tratamento de resultados e mensagens
    private $Result;
    private $Error;
//Entrada de dados
    private $Data;
/**
* <b>Responsável por cadastrar os dados no banco, a entrada deve ser um array envelopado
* @param array $Data
*/
public function ExeCreate(array $Data) {
    $this->Data = $Data;
    $this->CheckData();
    if ($this->Result):
        $this->Create();
    endif;
}
public function ExeUpdate(array $Data) {
    $this->Data = $Data;
    $this->CheckData();
    if ($this->Result):
        $this->Update();
    endif;
}
/**
* <b>Retorno de consulta</b>
* Se não houve consulta ele retorna true boleano ou false para erros
*/
public function getResult() {
    return $this->Result;
}
/**
* <b>Mensagens do sistema</b>
* Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
* @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela.
*/
public function getError() {
    return $this->Error;
}
########################################
########### METODOS PRIVADOS ###########
########################################
//Verifica a integridade dos dados e direciona as operações
private function CheckData() {
    if (in_array('', $this->Data)):
        $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Aviso!");
        $this->Result = false;
    else:
        $this->Data['notas_date'] = date('Y-m-d H:s:i');
        $this->Result = true;
    endif;
}
//Cadastra os dados do relatorio no banco
private function Create() {
    $Create = new Create;
    $Create->ExeCreate(TB_NOTAS, $this->Data);
    if (!$Create->getResult()):
        $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Aviso!");
        $this->Result = false;
    else:
        $this->Error = array("Cadastro realizado com sucesso.", WS_ACCEPT, "Aviso!");
        Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Notas", "Cadastrou a nota: {$this->Data['notas_titulo']}", date("Y-m-d H:i:s"));
        $this->Data = null;
        $this->Result = true;
    endif;
}
//
private function Update() {
    $Update = new Update;
    $Update->ExeUpdate(TB_NOTAS, $this->Data, "WHERE notas_id = :id", "id={$this->Data['notas_id']}");
    if (!$Update->getResult()):
        $this->Result = false;
    else:
        $this->Result = true;
        Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Notas", "Atualizou a nota ID #{$this->Data['notas_id']} Titulo: {$this->Data['notas_titulo']}", date("Y-m-d H:i:s"));
        $this->Error = array("A Nota <b>{$this->Data['notas_titulo']}</b>, foi atualizado com sucesso.", WS_ACCEPT, "Aviso!");
    endif;
}
}