<select name="user_id" class="form-control col-md-7 col-xs-12" required="required">
    <?php
        $ReadRecursos = new Read;
        $ReadRecursos->ExeRead(TB_USERS, "WHERE user_id != 1");
    ?>
    <option value="" selected="selected" class="text-info text-bold" style="padding: 10px;">-- Selecione --</option>
    <?php
    if ($ReadRecursos->getResult()):
        foreach ($ReadRecursos->getResult() as $key):
            ?>
            <option value="<?= $key['user_id'] ?>" <?php
                if (isset($post['user_id']) && $post['user_id'] == $key['user_id']): echo 'selected="selected"';
                elseif (isset($dados) && $dados['user_id'] == $key['user_id']): echo 'selected="selected"';
                endif; ?> 
            ><?= $key['user_name'] ?>
            </option>  
        <?php endforeach;
    endif;
    ?>
</select>