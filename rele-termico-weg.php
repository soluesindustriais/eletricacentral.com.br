<? $h1 = "Relé térmico weg"; $title  = "Relé térmico weg"; $desc = "Receba os valores médios de $h1, você só encontra na ferrementa Soluções Industriais, receba diversas cotações hoje com aproximadamente 100 fábricas ao mesmo tempo"; $key  = "Relé elétrico, Relé térmico de sobrecarga"; include('inc/rele-eletronico/rele-eletronico-linkagem-interna.php'); include('inc/head.php');  ?> <!-- Tabs Regiões --> <script defer src="<?=$url?>js/organictabs.jquery.js">  </script> <script async src="<?=$url?>inc/rele-eletronico/rele-eletronico-eventos.js"></script> </head> <body> <? include('inc/topo.php');?> <div class="wrapper"> <main  > <div class="content"> <section> <?=$caminhorele_eletronico?> <? include('inc/rele-eletronico/rele-eletronico-buscas-relacionadas.php');?> <br class="clear" /><h1><?=$h1?></h1> <article> <p><?=$desc?></p><p>Possuindo dezenas de fabricantes, o Soluções Industriais é a plataforma business to business mais interativo da área industrial. Para realizar um orçamento de <?=$h1?>, selecione um ou mais dos fornecedores abaixo:</p><hr /> <? include('inc/rele-eletronico/rele-eletronico-produtos-premium.php');?> <? include('inc/produtos-fixos.php');?> <? include('inc/rele-eletronico/rele-eletronico-imagens-fixos.php');?> <? include('inc/produtos-random.php');?>
<div class="tabela-tecnica" style=" margin-bottom: 35px;">
 <h2 >
	<strong><em style="font-style: normal;">Relé Térmico WEG</em></strong>
</h2>
<p>
A função do relé térmico weg é agir desligando o motor elétrico anteriormente que ele atinja o ponto limite de deterioração. 
Esse dispositivo possui montagem direta aos contatores ou também em base individual, com classe de disparo 10 e 3 polos.
</p>
<h2>
INFORMAÇÕES TÉCNICAS
</h2>
<img src="<?=$url?>imagens/rele-termico-weg.jpg" alt="Relé Térmico WEG" title="Relé Térmico WEG" style="float: left;">

<ul class="lista-tecnica">
<li> <i class="fas fa-angle-right"> </i>
Descrição: RW17-2D3-U017
  </li>
<li> <i class="fas fa-angle-right"> </i>
Código: 12450912
  </li>
<li> <i class="fas fa-angle-right"> </i>
Indicação de montagem: 2D
  </li>
<li> <i class="fas fa-angle-right"> </i>
Faixa de corrente: 11 - 17 A
  </li>
<li> <i class="fas fa-angle-right"> </i>
Número de polos: 3 polos
  </li>
<li> <i class="fas fa-angle-right"> </i>
Tensão nominal de isolação: Ui - IEC/EN 60947-4-1, VDE 0660; 690 V
  </li>
<li> <i class="fas fa-angle-right"> </i>
Tensão nominal de isolação: Ui - UL, CSA; 600 V
  </li>
<li> <i class="fas fa-angle-right"> </i>
Tensão nominal de impulso: Uimp (IEC/EN 60947-1); 6 kV
  </li>

</ul>
<ul style="margin:30px 10px;">
	<li> <i class="fas fa-angle-right"> </i>
Frequência: 25...400Hz
  </li>
	
	<li> <i class="fas fa-angle-right"> </i>
Uso em corrente contínua: Sim <img src="<?=$url?>imagens/rele-termico.jpg" alt="Relé Térmico" title="Relé Térmico" style="float: right; margin-top:-65px;">
  </li>
<li> <i class="fas fa-angle-right"> </i>
Frequência máxima de ciclos de manobra: 15 ops./h
  </li>
<li> <i class="fas fa-angle-right"> </i>
Grau de proteção - terminais principais: IP10
  </li>
<li> <i class="fas fa-angle-right"> </i>
Grau de proteção - contatos auxiliares: IP20
  </li>
<li> <i class="fas fa-angle-right"> </i>
Proteção contra curto-circuito: Com fusível (gL/gG); 6gL/gG
  </li>
<li> <i class="fas fa-angle-right"> </i>
Altura: 72 mm
  </li>
<li> <i class="fas fa-angle-right"> </i>
Largura: 45 mm
  </li>
<li> <i class="fas fa-angle-right"> </i>
Profundidade: 82 mm
  </li>
<li> <i class="fas fa-angle-right"> </i>
Peso bruto: 0,165 kg
  </li>
</ul>
</div>
   <? include('inc/rele-eletronico/rele-eletronico-galeria-videos.php');?> <hr /> <h2>Galeria de Imagens Ilustrativas referente a <?=$h1?></h2>  <? include('inc/rele-eletronico/rele-eletronico-galeria-fixa.php');?> <span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span></article> <? include('inc/rele-eletronico/rele-eletronico-coluna-lateral.php');?><br class="clear"><? include('inc/form-mpi.php');?><? include('inc/regioes.php');?> </section> </div> </main> </div><!-- .wrapper --> <? include('inc/footer.php');?> </body> </html>