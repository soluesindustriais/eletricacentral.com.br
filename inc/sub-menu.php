    <li class="dropdown-2">
      <a href="<?= $url ?>atuadores-eletricos-categoria" title="Atuador Elétrico">Atuador Elétrico</a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/atuadores-eletricos/atuadores-eletricos-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>capacitor-eletrolitico-categoria" title="Capacitores Eletrolítico">Capacitores Eletrolítico</a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/capacitor-eletrolitico/capacitor-eletrolitico-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>chave-eletronica-categoria" title="Chaves Eletrônica">Chaves Eletrônica</a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/chave-eletronica/chave-eletronica-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>componente-eletronico-categoria" title="Componentes Eletrônico ">Componentes Eletrônico </a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/componente-eletronico/componente-eletronico-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>condutores-eletricos-categoria" title="Condutor Elétrico ">Condutor Elétrico </a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/condutores-eletricos/condutores-eletricos-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>conector-eletrico-categoria" title="Conectores Elétricos ">Conectores Elétricos </a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/conector-eletrico/conector-eletrico-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>distribuidora-de-eletronicos-categoria" title="Distribuidoras de Eletrônicos ">Distribuidoras de Eletrônicos </a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/distribuidora-de-eletronicos/distribuidora-de-eletronicos-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>eletrica-industrial-categoria" title="Elétrica para Industrial ">Elétrica para Industrial </a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/eletrica-industrial/eletrica-industrial-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>motor-eletrico-categoria" title="Motores Elétrico">Motores Elétrico</a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/motor-eletrico/motor-eletrico-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>rele-eletronico-categoria" title="Relés Eletrônico ">Relés Eletrônico </a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/rele-eletronico/rele-eletronico-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>sensores-eletricos-categoria" title="Sensor Elétrico ">Sensor Elétrico </a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/sensores-eletricos/sensores-eletricos-sub-menu.php'); ?>
      </ul>
    </li>
    <li class="dropdown-2">
      <a href="<?= $url ?>dissipador-de-calor-categoria" title="Dissipador de calor">Dissipador de calor </a>
      <ul class="sub-menu2 lista-interna">
        <? include('inc/dissipador-de-calor/dissipador-de-calor-sub-menu.php'); ?>
      </ul>
    </li>