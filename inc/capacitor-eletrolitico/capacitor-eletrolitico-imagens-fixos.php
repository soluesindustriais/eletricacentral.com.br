<div class="grid">
  <div class="col-6">
    <div class="picture-legend picture-center"><a class="lightbox" href="<?= $url ?>imagens/capacitor-eletrolitico/capacitor-eletrolitico-01.jpg" title="<?= $h1 ?>" target="_blank"><img class="lazyload" data-src="<?= $url ?>imagens/capacitor-eletrolitico/thumbs/capacitor-eletrolitico-01.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>" /></a><strong>Imagem ilustrativa de <?= $h1 ?></strong></div>
  </div>
  <div class="col-6">
    <div class="picture-legend picture-center"><a class="lightbox" href="<?= $url ?>imagens/capacitor-eletrolitico/capacitor-eletrolitico-02.jpg" title="<?= $h1 ?>" target="_blank"><img class="lazyload" data-src="<?= $url ?>imagens/capacitor-eletrolitico/thumbs/capacitor-eletrolitico-02.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>" /></a><strong>Imagem ilustrativa de <?= $h1 ?></strong></div>
  </div>
</div>