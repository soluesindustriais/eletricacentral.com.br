<?
$nomeSite			= 'Eletrica Central';
$slogan				= 'Dezenas de produtos e serviços para componenter eletrônicos';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
	$url = $http . "://" . $host . "/";
} else {
	$url = $http . "://" . $host . $dir["dirname"] . "/";
}

$ddd				= '11';
$fone				= '9999-0000';
// $fone2				= '2222-4444';
// $fone3				= '2123-4444';
$emailContato		= 'everton.doutoresdaweb@gmail.com';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idCliente			= '123456778';
$idAnalytics		= 'UA-117773056-1';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php', '', $urlPagina);
$urlPagina 			== "index" ? $urlPagina = "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
// $siteKey = '6Ld4lksUAAAAACDi6rF7itm3pJ1-JMK5GOpsegof';
// $secretKey = '6Ld4lksUAAAAANOW3EXcE9MKBSrK4N65WkFGLOlX';


//Pasta para imagens, Galeria, url Facebook, etc.
$pasta 				= 'imagens/informacoes/';
//Redes sociais
$idFacebook			= 'Colocar o ID da página do Facebook'; //Link para achar o ID da página do Facebook http://graph.facebook.com/Nome da página do Facebook
$idGooglePlus		= 'http://plus.google.com.br'; // ID da página da empresa no Google Plus
// $paginaFacebook		= 'PAGINA DO FACEBOOK DO CLIENTE';
$author = ''; // Link do perfil da empresa no g+ ou deixar em branco
//Reescrita dos links dos telefones
$link_tel = str_replace('(', '', $ddd);
$link_tel = str_replace(')', '', $link_tel);
$link_tel = str_replace('11', '', $link_tel);
$link_tel .= '5511' . $fone;
$link_tel = str_replace('-', '', $link_tel);
$creditos			= 'Doutores da Web - Marketing Digital';
$siteCreditos		= 'www.doutoresdaweb.com.br';


//Breadcrumbs
$caminho 			= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

$caminho2 			= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" itemprop="url" href="' . $url . '" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
</div>
';

$caminhoInfo	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';
$caminhoProdutos	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'buscar" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

// Produtos PRINCIPAL
$caminhoprodutos	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

// ESTAÇÃO DE TRATAMENTO
$caminhoatuadores_eletricos	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'atuadores-eletricos-categoria" title="Atuadores Elétricos" class="category" itemprop="url"><span itemprop="title">Atuadores Elétricos </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

// TRATAMENTO DE ÁGUA
$caminhocapacitor_eletrolitico	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'capacitor-eletrolitico-categoria" title="Capacitor Elétrolitico" class="category" itemprop="url"><span itemprop="title">Capacitor Elétrolitico </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';


// TRATAMENTO DE EFLUENTES
$caminhochave_eletronica	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'chave-eletronica-categoria" title="Chave Eletrônica" class="category" itemprop="url"><span itemprop="title">Chave Eletrônica </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';


// TRATAMENTO DE ESGOTO
$caminhocomponente_eletronico	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'componente-eletronico-categoria" title="Componente Eletrônico" class="category" itemprop="url"><span itemprop="title">Componente Eletrônico </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';


// TRATAMENTO DE RESIDUOS
$caminhocondutores_eletricos	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'condutores-eletricos-categoria" title="Condutores Elétricos" class="category" itemprop="url"><span itemprop="title">Condutores Elétricos </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

// TRATAMENTO DE RESIDUOS
$caminhoconector_eletrico	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'conector-eletrico-categoria" title="Conector Elétrico" class="category" itemprop="url"><span itemprop="title">Conector Elétrico </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

// TRATAMENTO DE RESIDUOS
$caminhodistribuidora_de_eletronicos	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'distribuidora-de-eletronicos-categoria" title="Distribuidora de Eletrônicos" class="category" itemprop="url"><span itemprop="title">Distribuidora de Eletrônicos </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

// TRATAMENTO DE RESIDUOS
$caminhomotor_eletrico	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'eletrica-industrial-categoria" title="Motor Elétrico" class="category" itemprop="url"><span itemprop="title">Motor Elétrico </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

// TRATAMENTO DE RESIDUOS
$caminhoeletrica_industrial	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'eletrica-industrial-categoria" title="Elétrica Industrial" class="category" itemprop="url"><span itemprop="title">Elétrica Industrial </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';
// TRATAMENTO DE RESIDUOS
$caminhorele_eletronico	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'rele-eletronico-categoria" title="Relé Eletrônico" class="category" itemprop="url"><span itemprop="title">Relé Eletrônico </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

// TRATAMENTO DE RESIDUOS
$caminhosensores_eletricos	= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'rele-eletronico-categoria" title="Sensores Elétricos" class="category" itemprop="url"><span itemprop="title">Sensores Elétricos </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';
$caminhocapacitores_eletrolitico		= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'rele-eletronico-categoria" title="Capacitores Eletrolitico" class="category" itemprop="url"><span itemprop="title">Capacitores Eletrolitico </span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

$caminhodissipador_de_calor		= '
<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<a href="' . $url . 'dissipador-de-calor-categoria" title="Dissipador de calor" class="category" itemprop="url"><span itemprop="title">Dissipador de calor	</span></a> »
		<div itemprop="child" itemscope itemtype="https://schema.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';
