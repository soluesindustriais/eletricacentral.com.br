<footer>
    <div class="wrapper-footer">
        <div class="contact-footer">
            <address>
                <span><?= $nomeSite . " - " . $slogan ?></span>
            </address>
            <br>
        </div>
        <div class="menu-footer">
            <nav>
                <ul>
                    <li><a href="<?= $url ?>" title="Página inicial">Inicio</a></li>
                    <li><a href="<?= $url ?>produtos" title="produtos <?= $nomeSite ?>">Produtos</a></li>
                    <li><a href="<?= $url ?>sobre-nos" title="Sobre nós<?= $nomeSite ?>">Sobre nós</a></li>
                    <li><a href="<?= $url ?>mapa-site" title="Mapa do Site <?= $nomeSite ?>">Mapa do Site</a></li>

                </ul>
            </nav>
        </div>
    </div>



</footer>
<div class="copyright-footer">
    <div class="wrapper-copy">
        Copyright ©
        <?= $nomeSite ?>. (Lei 9610 de 19/02/1998)
        <div class="center-footer img-logo">
            <img src="<?= $url ?>imagens/logo.png" alt="Elétrica Central" title="Elétrica Central">
            <p>é um parceiro</p>
            <img src="<?= $url ?>imagens/logo-solucs.png" alt="Soluções Industriais" title="Soluções Industriais">
        </div>
        <div class="selos">
            <a rel="nofollow noopener" href="https://validator.w3.org/check?uri=<?= $url . $urlPagina ?>" target="_blank"
                title="HTML5 W3C"><i class="fab fa-html5"></i> <strong>W3C</strong></a>
            <a rel="nofollow noopener" href="https://jigsaw.w3.org/css-validator/validator?uri=<?= $url . $urlPagina ?>"
                target="_blank" title="CSS W3C"><i class="fab fa-css3"></i> <strong>W3C</strong></a>

        </div>
    </div>
</div>
<script defer src="<?= $url ?>js/geral.js"></script>
<? ?>
<script>
    $(".lightbox").fancybox({})
</script>

<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>


<script>
    var guardar = document.querySelectorAll('.botao-cotar');
    for (var i = 0; i < guardar.length; i++) {
        guardar[i].removeAttribute('href');
        var adicionando = guardar[i].parentNode;
        adicionando.classList.add('nova-api');
    };
</script>


<!-- Google Analytics 4 -->
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-639C54QG2B"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-639C54QG2B');
</script>



<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-D23WW3S4NC');
</script>


<!-- Google Analytics 4  FIM -->



<!-- BOTAO SCROLL -->
<script src="<?= $url ?>js/jquery.scrollUp.min.js"></script>
<script src="<?= $url ?>js/scroll.js"></script>


<!-- /BOTAO SCROLL -->
<script async src="js/click-actions.js"></script>
<script async src="<?= $url ?>js/vendor/modernizr-2.6.2.min.js"></script>
<script async src="<?= $url ?>js/app.js"></script>


<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script><?php include 'inc/fancy.php'; ?><div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>

<script>
    function toggleDetails() {
        var detailsElement = document.querySelector(".webktbox");

        // Verificar se os detalhes estão abertos ou fechados
        if (detailsElement.hasAttribute("open")) {
            // Se estiver aberto, rolar suavemente para cima
            window.scrollTo({
                top: 200,
                behavior: "smooth"
            });
        } else {
            // Se estiver fechado, rolar suavemente para baixo (apenas 100px)
            window.scrollTo({
                top: 1300,
                behavior: "smooth"
            });
        }
    }
</script>