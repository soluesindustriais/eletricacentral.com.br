<? $h1 = "Controlador de temperatura"; $title  = "Controlador de temperatura"; $desc = "Compare $h1, você só adquire nos resultados das buscas do Soluções Industriais, realize um orçamento online com aproximadamente 200 fabricantes de todo o Brasil"; $key  = "Resistor de alta potência, Barra de pinos dupla"; include('inc/componente-eletronico/componente-eletronico-linkagem-interna.php'); include('inc/head.php');  ?> <!-- Tabs Regiões --> <script defer src="<?=$url?>js/organictabs.jquery.js">  </script> <script async src="<?=$url?>inc/componente-eletronico/componente-eletronico-eventos.js"></script> </head> <body> <? include('inc/topo.php');?> <div class="wrapper"> <main  > <div class="content"> <section> <?=$caminhocomponente_eletronico?> <? include('inc/componente-eletronico/componente-eletronico-buscas-relacionadas.php');?> <br class="clear" /><h1><?=$h1?></h1> <article> <p><?=$desc?></p><p>Possuindo centenas de distribuidores, o Soluções Industriais é o facilitador B2B mais interativo do ramo. Para receber uma cotação de <?=$h1?>, selecione uma ou mais das empresas a seguir:</p><hr /> <? include('inc/componente-eletronico/componente-eletronico-produtos-premium.php');?> <? include('inc/produtos-fixos.php');?> <? include('inc/componente-eletronico/componente-eletronico-imagens-fixos.php');?> <? include('inc/produtos-random.php');?> <div class="tabela-tecnica" style=" margin: 5px;
    border: 1px solid #d7d7d7;
    background-color: #fff; max-width: 100%; height: auto; padding:10px;">
    <h2>	
 Chave Seccionadora Tripolar
</h2>

<p>	
Chave seccionadora de média tensão para uso externo, tripolar, operação sem carga e fabricada de acordo com as normas ANSI e ABNT NBR IEC 62271-102.
</p>

<p>	
Cada polo é constituído de duas colunas de isoladores laterais, uma do tipo suporte e uma isoladorora central do tipo bastão. Os contatos principais móveis são do tipo dupla faca, e os contatos fixos são dispostos de forma a suportar esforços resultantes das solicitações eletrodinâmicas.
</p>

<h2>	
Principais Características:
</h2>

<p>	
As chaves seccionadoras tripolares são fabricadas segundo modernas tecnologias de processo e materiais.
</p>

<p>	
A chave seccionadora de abertura vertical foi desenvolvida para atender especificamente BANCOS DE CAPACITORES, podendo ter outras aplicações no sistema elétrico;
</p>
<ul>	
<li>	
Terminais de liga de cobre eletrolítico de alta condutividade elétrica, com furação normalizada (NEMA);
</li>
<li>	
Lâminas principais de liga de cobre eletrolítico de alta condutividade elétrica e resistência mecânica;
</li>
<li>	
Contatos de cobre eletrolítico estanhados ou prateados, de alta condutividade;
</li>
<li>	
A base é fabricada com chapa dobrada de aço carbono galvanizada a fogo;
</li>
<li>	
Isoladores de porcelana ou em resina epóxi cicloalifática uso EXTERNO (fabricação própria), com alta resistência mecânica e distância de escoamento até 36mm/kV;
</li>
<li>	
Montagem em base única;
</li>
<li>	
Abertura Vertical;
</li>
<li>	
Montagem horizontal, vertical ou invertida;
</li>
<li>	
Conector de aterramento (25 a 70mm²).
</li>
</ul>

	</div> <hr />  <? include('inc/componente-eletronico/componente-eletronico-galeria-videos.php');?> <hr /> <h2>Galeria de Imagens Ilustrativas referente a <?=$h1?></h2>  <? include('inc/componente-eletronico/componente-eletronico-galeria-fixa.php');?> <span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span></article> <? include('inc/componente-eletronico/componente-eletronico-coluna-lateral.php');?><br class="clear"><? include('inc/form-mpi.php');?><? include('inc/regioes.php');?> </section> </div> </main> </div><!-- .wrapper --> <? include('inc/footer.php');?> </body> </html>