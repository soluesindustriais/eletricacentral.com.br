<? $h1 = "Capacitor eletrolítico";
$title  = "Capacitor eletrolítico";
$desc = "Compare $h1, você só adquire nos resultados das buscas do Soluções Industriais, realize um orçamento online com aproximadamente 200 fabricantes de todo o Brasil";
$key  = "Capacitor trifásico weg, Capacitor eletrolítico smd";
include('inc/capacitor-eletrolitico/capacitor-eletrolitico-linkagem-interna.php');
include('inc/head.php');  ?> <!-- Tabs Regiões -->
<script defer src="<?= $url ?>js/organictabs.jquery.js"> </script>
<script async src="<?= $url ?>inc/capacitor-eletrolitico/capacitor-eletrolitico-eventos.js"></script>
</head>

<body> <? include('inc/topo.php'); ?> <div class="wrapper">
		<main>
			<div class="content">
				<section> <?= $caminhocapacitor_eletrolitico ?> <? include('inc/capacitor-eletrolitico/capacitor-eletrolitico-buscas-relacionadas.php'); ?> <br class="clear" />
					<h1><?= $h1 ?></h1>
					<article>
						<p><?= $desc ?></p>
						<p>Possuindo centenas de distribuidores, o Soluções Industriais é o facilitador B2B mais interativo do ramo. Para receber uma cotação de <?= $h1 ?>, selecione uma ou mais das empresas a seguir:</p>
						<hr /> <? include('inc/capacitor-eletrolitico/capacitor-eletrolitico-produtos-premium.php'); ?> <? include('inc/produtos-fixos.php'); ?>

						<? include('inc/capacitor-eletrolitico/capacitor-eletrolitico-imagens-fixos.php'); ?> <? include('inc/produtos-random.php'); ?><div class="tabela-tecnica" style=" margin: 5px;
    border: 1px solid #d7d7d7;
    background-color: #fff; max-width: 100%; height: auto; padding:10px;">
							<h2>Tabela Técnica</h2>
							<h3><strong><em>Capacitor Eletrolítico</em></strong></h3>

							<table style="border:0px; color:rgb(102, 102, 102); font-family:open sans,verdana,geneva,sans-serif; font-size:14px; margin:10px 0px 0px; padding:0px; position:relative; ">
								<thead>
									<tr>
										<th style="text-align:left; width:560px"><span style="color:#333">Ala dos Capacitores Eletrolíticos Convencionais</span></th>
										<th style="text-align:left; width:136px"><span style="color:#333">Quantidade</span></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="border:1px solid #333; padding:5px"> CAPAC. ELETROLÍTICO 10/400V (CD11) 105C LSECON (10x13x5,0) FITADO</td>
										<td style="border:1px solid #333; padding:5px">120.000,00</td>
									</tr>
									<tr>
										<td style="border:1px solid #333; padding:5px"> CAPAC. ELETROLÍTICO 470/10V (CD11) 105C LSECON (6,3x15x2,5) FITADO</td>
										<td style="border:1px solid #333; padding:5px">10.000,00</td>
									</tr>
									<tr>
										<td style="border:1px solid #333; padding:5px"> CAPAC. ELETROLÍTICO 470/10V (CD11) 105C LSECON (6,3x15x2,5) FITADO</td>
										<td style="border:1px solid #333; padding:5px">140.000,00</td>
									</tr>
									<tr>
										<td style="border:1px solid #333; padding:5px"> CAPAC. ELETROLÍTICO 33/400V (CD11) 105C LSECON (16x20) (100PCS)</td>
										<td style="border:1px solid #333; padding:5px">50.000,00</td>
									</tr>
								</tbody>
							</table>
						</div>
						<hr />
						<h2>Veja algumas referências de <?= $h1 ?> no youtube</h2> <? include('inc/capacitor-eletrolitico/capacitor-eletrolitico-galeria-videos.php'); ?>
						<hr />
						<h2>Galeria de Imagens Ilustrativas referente a <?= $h1 ?></h2> <? include('inc/capacitor-eletrolitico/capacitor-eletrolitico-galeria-fixa.php'); ?> <span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span>
					</article> <? include('inc/capacitor-eletrolitico/capacitor-eletrolitico-coluna-lateral.php'); ?><br class="clear"><? include('inc/form-mpi.php'); ?><? include('inc/regioes.php'); ?>
				</section>
			</div>
		</main>
	</div><!-- .wrapper --> <? include('inc/footer.php'); ?> </body>

</html>