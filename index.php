<?
$h1         = 'Componentes Elétricos';
$title      = 'Cotações de componentes eletrônicos -  Componentes Elétricos';
$desc       = 'Procurando Componentes Elétricos? Aqui você encontra os melhores distribuidores para todos os tipos de componentes eletrônicos!';
$key        = 'Componentes elétricos, componentes eletrônicos, resistores';
$var        = 'Home';
include('inc/head.php');

?>
<!-- <script src="<?=$url?>js/owl.carousel.js" ></script>
<script src="<?=$url?>js/slide-carousel.js" ></script>
<link rel="stylesheet" href="<?=$url?>css/owl.carousel.css">
<link rel="stylesheet" href="<?=$url?>css/owl.theme.css"> -->
</head>
<body>
<? include('inc/topo.php'); ?>
  <div class="wrapper-intro">
    <div class="wrapper">
      <div class="intro">
                      <div class="main-content">
                 <h1>Componentes Elétricos</h1>
                 <h2>Regulador de Tensão</h2>
                 <p>Transforme sua energia com o nosso Regulador de Tensão. Faça sua instalação elétrica mais segura e eficiente agora! </p>
                 <span class="btn-space" >
                    <button onClick="location.href='<?=$url?>regulador-de-tensao'" class="button__label btn-saiba wow animated fadeIn" data-wow-delay=".9s" data-s-animation="button" data-animation-complete=""><b>Saiba Mais</b>
                    </button>
                 </span>
      </div>
    </div>
                   <span class="scroll-down" data-anime="down">
            <a ></a><i class="fa fa-angle-down fa-3x arrow "></i>
         </span>
  </div>
</div>
<main>
     <section class="wrapper-main">
      <div class="main-center">
         <div class=" quadro-2 ">
            <h2>Componentes Eletrônicos</h2>
            <div class="div-img"> 
               <p data-anime="left-0">Entre os itens que compõem o sistema elétrico o <a href="<?=$url?>componentes-eletronicos">componente eletrônico</a> ganha destaque por minimizar o circuito. Esses tipos de itens são recomendados para usar de maneira pré determinada pela empresa fabricante e a instalação deve ser feita por profissionais.
 </p>
            </div>
            <div class="gerador-svg" data-anime="in">
               <img src="imagens/componente-eletrolitico.jpg" alt="Componente Eletrolítico" title="Componente Eletrolítico">
            </div>
         </div>
         <div class=" incomplete-box" >
            <ul data-anime="in">
               <li>
                  <p>Os componentes eletrônicos são a estrutura de um circuito eletrônico e podem ser definidos como componente eletrônico todo dispositivo eléctrico que transmite a corrente eléctrica através de um condutor ou semicondutor. Dentre a repleta gama de produtos utilizados nos circuitos é importante destacar os seguintes componentes elétricos:</p>
                  <li><i class="fas fa-minus"></i> Capacitores</li>
                  <li><i class="fas fa-minus"></i> Resistores</li>
                  <li><i class="fas fa-minus"></i> Varistores</li>
                  <li><i class="fas fa-minus"></i> Barra de Pinos Conectores</li>
                  <li><i class="fas fa-minus"></i> Entre muitos outros.</li>
               </ul>
               <a href="<?=$url?>componentes-eletronicos" class="btn-4">Saiba mais</a>
            </div>
         </div>
         <div id="content-icons">
            <div class="co-icon">
               <div class="quadro-icons" data-anime="in">
                 <a href="https://www.eletricacentral.com.br/reed-switch-preco"><img src="imagens/resistores.jpg"  alt="Resistores" title="Resistores"></a>
                  <div>   <h3><a href="https://www.eletricacentral.com.br/reed-switch-preco">Resistores</a></h3>
                     <p>Conversores de energia elétrica em energia térmica.</p>
                  </div>
               </div>
            </div>
            <div class="co-icon">
               <div class="quadro-icons" data-anime="in">
                 <a href="https://www.eletricacentral.com.br/capacitor-eletrolitico"> <img src="imagens/resistor.jpg" alt="Componente Eletrolítico" title="Componente Eletrolítico"></a>
                  <div>
                     <h3> <a href="https://www.eletricacentral.com.br/capacitor-eletrolitico"> Componente Eletrolítico </a></h3>
                     <p>Componentes de armazenamento elétrico por meio líquido.</p>
                  </div>
               </div>   
            </div>
            <div class="co-icon">
               <div class="quadro-icons" data-anime="in">
                <a href="https://www.eletricacentral.com.br/capacitor-eletronico"> <img src="imagens/capacitor-eletrico.jpg"  alt="Capacitor Elétrico" title="Capacitor Elétrico"></a>
                  <div>   <h3> <a href="https://www.eletricacentral.com.br/capacitor-eletronico">Capacitores </a></h3>
                     <p>Componentes elétricos que armazenam carga elétrica em um campo elétrico.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="wrapper-img">
                  <div class="wrapper">
                     <div class="destaque txtcenter">
                        <h2>Galeria de <b> Produtos</b></h2>
                        <div class="center-block txtcenter">
                           <ul class="gallery">
                              <li><a href="<?=$url?>imagens/eletrica-central/componentes-eletricos.jpg" class="lightbox" title="Componentes Elétricos">
                                 <img src="<?=$url?>imagens/eletrica-central/thumbs/componentes-eletricos.jpg" alt="Componentes Elétricos" title="Componentes Elétricos">
                              </a>
                           </li>
                              <li><a href="<?=$url?>imagens/eletrica-central/componentes-eletronicos.jpg" class="lightbox" title="Componentes Eletrônicos">
                                 <img src="<?=$url?>imagens/eletrica-central/thumbs/componentes-eletronicos.jpg" alt="Componentes Eletrônicos" title="Componentes Eletrônicos">
                           </a>
                        </li>
                              <li><a href="<?=$url?>imagens/capacitor-eletrolitico/capacitor-eletrolitico-3.jpg" class="lightbox" title="Capacitor Eletrolítico">
                                 <img src="<?=$url?>imagens/capacitor-eletrolitico/thumbs/capacitor-eletrolitico-3.jpg" alt="Capacitor Eletrolítico" title="Capacitor Eletrolítico">
                        </a>
                     </li>
                              <li><a href="<?=$url?>imagens/eletrica-central/comprar-componentes-eletrenicos.jpg" class="lightbox" title="Comprar Componentes Eletrônicos">
                                 <img src="<?=$url?>imagens/eletrica-central/thumbs/comprar-componentes-eletrenicos.jpg" alt="Comprar Componentes Eletrônicos" title="Comprar Componentes Eletrônicos">
                     </a>
                  </li>
                              <li><a href="<?=$url?>imagens/eletrica-central/comprar-componentes-eletricos.jpg" class="lightbox" title="Comprar Componentes Elétricos">
                                 <img src="<?=$url?>imagens/eletrica-central/thumbs/comprar-componentes-eletricos.jpg" alt="Comprar Componentes Elétricos"  title="Comprar Componentes Elétricos">
                  </a>
               </li>
                              <li><a href="<?=$url?>imagens/eletrica-central/conjunto-de-componentes-eletronicos.jpg" class="lightbox" title="Conjunto de componentes eletrônicos">
                                 <img src="<?=$url?>imagens/eletrica-central/thumbs/conjunto-de-componentes-eletronicos.jpg" alt="Conjunto de componentes eletrônicos" title="Conjunto de componentes eletrônicos">
               </a>
            </li>
                              <li><a href="<?=$url?>imagens/eletrica-central/cotar-componentes-eletricos.jpg" class="lightbox" title="Cotar Componentes Elétricos">
                                 <img src="<?=$url?>imagens/eletrica-central/thumbs/cotar-componentes-eletricos.jpg" alt="Cotar Componentes Elétricos" title="Cotar Componentes Elétricos">
            </a>
         </li>
                              <li><a href="<?=$url?>imagens/eletrica-central/cotacao-componentes-eletronicos.jpg" class="lightbox" title="Cotação Componentes Eletrônicos">
                                 <img src="<?=$url?>imagens/eletrica-central/thumbs/cotacao-componentes-eletronicos.jpg" alt="Cotação Componentes Eletrônicos" title="Cotação Componentes Eletrônicos">
         </a>
      </li>
</ul>
</div>
</div>
</div>
</section>
</main>
<? include('inc/footer.php'); ?>
</body>
</html>