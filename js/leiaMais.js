// function toggleReadMore() {
//    //seleciona a class principal da DIV e os botões "leia mais" e "fechar"
//         var content = document.querySelector('.article-content'); 
//         var readMoreButton = document.querySelector('.read-more-button');
//         var closeButton = document.querySelector('.close-button');

//     //verifica se o conteúdo está exposto pelo max-height
//         if (content.style.maxHeight) { 

//         //Se estiver visivel, ele esconde
//             content.style.maxHeight = null; // remove o max-height para voltar ao tamanho padrão
//             closeButton.style.display = 'none'; // oculta o botão 'Fechar'
//             readMoreButton.style.display = 'block'; // mostra o botão 'Leia Mais'
//         } else {

//         // Se estiver contraído, expande o elemento
//             content.style.maxHeight = "none"; // define o max-height como "none" para permitir que o conteúdo se ajuste automaticamente
//             closeButton.style.display = 'block'; // mostra o botão 'Fechar'
//             readMoreButton.style.display = 'none'; // esconde o botão 'Leia Mais'
//         }
//     }function toggleReadMore() {
//    //seleciona a class principal da DIV e os botões "leia mais" e "fechar"
//         var content = document.querySelector('.article-content'); 
//         var readMoreButton = document.querySelector('.read-more-button');
//         var closeButton = document.querySelector('.close-button');

//     //verifica se o conteúdo está exposto pelo max-height
//         if (content.style.maxHeight) { 

//         //Se estiver visivel, ele esconde
//             content.style.maxHeight = null; // remove o max-height para voltar ao tamanho padrão
//             closeButton.style.display = 'none'; // oculta o botão 'Fechar'
//             readMoreButton.style.display = 'block'; // mostra o botão 'Leia Mais'
//         } else {

//         // Se estiver contraído, expande o elemento
//             content.style.maxHeight = "none"; // define o max-height como "none" para permitir que o conteúdo se ajuste automaticamente
//             closeButton.style.display = 'block'; // mostra o botão 'Fechar'
//             readMoreButton.style.display = 'none'; // esconde o botão 'Leia Mais'
//         }
//     }

function toggleReadMore() {
    var content = document.querySelector('.article-content');
    var readMoreButton = document.querySelector('.read-more-button');
    var closeButton = document.querySelector('.close-button');

    if (content.style.maxHeight) {
        content.style.maxHeight = null;
        closeButton.style.display = 'none';
        readMoreButton.style.display = 'block';

    } else {
        content.style.maxHeight = "none";
        closeButton.style.display = 'block';
        readMoreButton.style.display = 'none';

        // Rola para baixo quando o botão "read-more-button" é clicado
        content.scrollIntoView({
            top: 1000,
            behavior: 'smooth',
            block: 'start'
        });
    }
}


function closeAndScroll() {
    // Chama a função toggleReadMore para fechar a div
    toggleReadMore();

    // Rola para cima quando o botão "close-button" é clicado
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });
}