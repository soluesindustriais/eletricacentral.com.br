<? $h1 = "Chave seccionadora tripolar"; $title  = "Chave seccionadora tripolar"; $desc = "Compare preços de $h1, conheça os melhores fabricantes, faça um orçamento pelo formulário com mais de 50 fornecedores"; $key  = "Chave fim de curso de segurança, Chave táctil 90 graus"; include('inc/chave-eletronica/chave-eletronica-linkagem-interna.php'); include('inc/head.php');  ?> <!-- Tabs Regiões --> <script defer src="<?=$url?>js/organictabs.jquery.js">  </script> <script async src="<?=$url?>inc/chave-eletronica/chave-eletronica-eventos.js"></script> </head> <body> <? include('inc/topo.php');?> <div class="wrapper"> <main  > <div class="content"> <section> <?=$caminhochave_eletronica?> <? include('inc/chave-eletronica/chave-eletronica-buscas-relacionadas.php');?> <br class="clear" /><h1><?=$h1?></h1> <article> <p><?=$desc?></p><p>Desenvolvido para compradores, a ferramenta Soluções Industriais selecionou a maior gama de produtos qualificados do ramo industrial. Caso você tenha interesse por <?=$h1?> e gostaria de informações sobre o anunciante clique em uma das empresas listados adiante: </p><hr /> <? include('inc/chave-eletronica/chave-eletronica-produtos-premium.php');?> <? include('inc/produtos-fixos.php');?> <? include('inc/chave-eletronica/chave-eletronica-imagens-fixos.php');?> <? include('inc/produtos-random.php');?><div class="tabela-tecnica" style=" margin: 5px;
    border: 1px solid #d7d7d7;
    background-color: #fff; max-width: 100%; height: auto; padding:10px 20px;">

<div>	
 <h2>	
 Chave Seccionadora Tripolar
</h2>
<img src="<?=$url?>imagens/chave-seccionadora.jpg" alt="Chave Seccionadora" title="Chave Seccionadora">
<img src="<?=$url?>imagens/chave-seccionadora-tripolar.jpg" alt="Chave Seccionadora Tripolar" title="Chave Seccionadora Tripolar">
<p>	
Chave seccionadora de média tensão para uso externo, tripolar, operação sem carga.
Cada polo é constituído de duas colunas de isoladores laterais, uma do tipo suporte e uma isoladorora central do tipo bastão. Os contatos principais móveis são do tipo dupla faca, e os contatos fixos são dispostos de forma a suportar esforços resultantes das solicitações eletrodinâmicas.
</p>

<h2>	
Principais Características:
</h2>

<p>	
As chaves seccionadoras tripolares são fabricadas segundo modernas tecnologias de processo e materiais.
</p>
<p>	
A chave seccionadora de abertura vertical foi desenvolvida para atender especificamente BANCOS DE CAPACITORES, podendo ter outras aplicações no sistema elétrico;
</p>

</div>
<ul>	
<li>	
- Terminais de liga de cobre eletrolítico de alta condutividade elétrica, com furação normalizada (NEMA);
</li>
<li>	
- Lâminas principais de liga de cobre eletrolítico de alta condutividade elétrica e resistência mecânica;
</li>
<li>	
- Contatos de cobre eletrolítico estanhados ou prateados, de alta condutividade;
</li>
<li>	
- A base é fabricada com chapa dobrada de aço carbono galvanizada a fogo;
</li>
<li>	
- Isoladores de porcelana ou em resina epóxi cicloalifática uso EXTERNO (fabricação própria), com alta resistência mecânica e distância de escoamento até 36mm/kV;
</li>
<li>	
- Montagem em base única;
</li>
<li>	
- Abertura Vertical;
</li>
<li>	
- Montagem horizontal, vertical ou invertida;
</li>
<li>	
- Conector de aterramento (25 a 70mm²).
</li>
</ul>


	</div> <hr />  <? include('inc/chave-eletronica/chave-eletronica-galeria-videos.php');?> <hr /> <h2>Galeria de Imagens Ilustrativas referente a <?=$h1?></h2>  <? include('inc/chave-eletronica/chave-eletronica-galeria-fixa.php');?> <span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span></article> <? include('inc/chave-eletronica/chave-eletronica-coluna-lateral.php');?><br class="clear"><? include('inc/form-mpi.php');?><? include('inc/regioes.php');?> </section> </div> </main> </div><!-- .wrapper --> <? include('inc/footer.php');?> </body> </html>