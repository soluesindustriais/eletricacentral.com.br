<?
$h1         = 'SELECIONE A CATEGORIA DESEJADA';
$title      = 'Faça cotações com diversas empresas';
$desc       = 'Elétrica Central - Faça cotações com diversas empresas de Componentes elétricos gratuitamente';
$key        = 'Produtos, componentes elétricos, componentes eletrônicos, capacitores';
$var        = 'Elétrica Central';
include('inc/head.php');
?>
</head>

<body>
  <? include('inc/topo.php'); ?>
  <div class="wrapper">
    <main>
      <div class="content">
        <div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb">
          <a rel="home" itemprop="url" href="https://www.eletricacentral.com.br" title="home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> home</span></a> »
          <strong><span class="page" itemprop="title">Produtos</span></strong>
        </div>
        <h1>Elétrica Central</h1>
        <article class="full">
          <h2>PRODUTOS - SELECIONE A CATEGORIA DESEJADA</h2>
          <ul class="thumbnails-main">
            <li>
              <a rel="nofollow" href="<?= $url ?>atuadores-eletricos-categoria" title="Atuadores Elétricos"><img src="<?= $url ?>imagens/atuadores-eletricos/thumbs/atuadores-eletricos-01.jpg" alt="Atuadores Elétricos" title="Atuadores Elétricos" /></a>
              <h2><a href="<?= $url ?>atuadores-eletricos-categoria" title="Atuadores Elétricos">Atuadores Elétricos</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url ?>capacitor-eletrolitico-categoria" title="Capacitor Eletrolítico"><img src="<?= $url ?>imagens/capacitor-eletrolitico/thumbs/capacitor-eletrolitico-02.jpg" alt="Capacitor Eletrolítico" title="Capacitor Eletrolítico" /></a>
              <h2><a href="<?= $url ?>capacitor-eletrolitico-categoria" title="Capacitor Eletrolítico">Capacitor Eletrolítico</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url ?>chave-eletronica-categoria" title="Chave Eletrônica"><img src="<?= $url ?>imagens/chave-eletronica/thumbs/chave-eletronica-7.jpg" alt="Chave Eletrônica" title="Chave Eletrônica" /></a>
              <h2><a href="<?= $url ?>chave-eletronica-categoria" title="Chave Eletrônica">Chave Eletrônica</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url ?>componente-eletronico-categoria" title="Componente Eletrônico"><img src="<?= $url ?>imagens/componente-eletronico/thumbs/componente-eletronico-01.jpg" alt="Componente Eletrônico" title="Componente Eletrônico" /></a>
              <h2><a href="<?= $url ?>componente-eletronico-categoria" title="Componente Eletrônico">Componente Eletrônico</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url ?>condutores-eletricos-categoria" title="Condutores Elétricos"><img src="<?= $url ?>imagens/condutores-eletricos/thumbs/condutores-eletricos-01.jpg" alt="Condutores Elétricos" title="Condutores Elétricos" /></a>
              <h2><a href="<?= $url ?>condutores-eletricos-categoria" title="Condutores Elétricos">Condutores Elétricos</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url ?>conector-eletrico-categoria" title="Conector Elétrico"><img src="<?= $url ?>imagens/conector-eletrico/conector-eletrico-01.jpg" alt="Conector Elétrico" title="Conector Elétrico" /></a>
              <h2><a href="<?= $url ?>conector-eletrico-categoria" title="Conector Elétrico">Conector Elétrico</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url ?>distribuidora-de-eletronicos-categoria" title="Distribuidora de Eletrônicos"><img src="<?= $url ?>imagens/distribuidora-de-eletronicos/thumbs/distribuidora-de-eletronicos-01.jpg" alt="Distribuidora de Eletrônicos" title="Distribuidora de Eletrônicos" /></a>
              <h2><a href="<?= $url ?>distribuidora-de-eletronicos-categoria" title="Distribuidora de Eletrônicos">Distribuidora de Eletrônicos</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url ?>eletrica-industrial-categoria" title="Elétrica Industrial"><img src="<?= $url ?>imagens/eletrica-industrial/thumbs/eletrica-industrial-1.jpg" alt="Elétrica Industrial" title="Elétrica Industrial" /></a>
              <h2><a href="<?= $url ?>eletrica-industrial-categoria" title="Elétrica Industrial">Elétrica Industrial</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url ?>motor-eletrico-categoria" title="Motor Elétrico"><img src="<?= $url ?>imagens/motor-eletrico/thumbs/motor-eletrico-01.jpg" alt="Motor Elétrico" title="Motor Elétrico" /></a>
              <h2><a href="<?= $url ?>motor-eletrico-categoria" title="Motor Elétrico">Motor Elétrico</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url ?>rele-eletronico-categoria" title="Rele Etlétrico"><img src="<?= $url ?>imagens/rele-eletronico/thumbs/rele-eletronico-2.jpg" alt="Relé Etlétrico" title="Relé Etlétrico" /></a>
              <h2><a href="<?= $url ?>rele-eletronico-categoria" title="Rele Etlétrico">Rele Etlétrico</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url ?>sensores-eletricos-categoria" title="Sensores Elétricos"><img src="<?= $url ?>imagens/sensores-eletricos/thumbs/sensores-eletricos-01.jpg" alt="Sensores Elétricos" title="Sensores Elétricos" /></a>
              <h2><a href="<?= $url ?>sensores-eletricos-categoria" title="Sensores Elétricos">Sensores Elétricos</a></h2>
            </li>

            <li>
              <a rel="nofollow" href="<?= $url ?>dissipador-de-calor-categoria" title="Dissipador de Calor"><img src="<?= $url ?>imagens/dissipador-de-calor/thumbs/dissipador-de-calor-01.jpg" alt="Dissipador de Calor" title="Dissipador de Calor" /></a>
              <h2><a href="<?= $url ?>dissipador-de-calor-categoria" title="Dissipador de Calor">Dissipador de Calor</a></h2>
            </li>

          </ul>
        </article>
      </div>
    </main>
  </div>
  <? include('inc/footer.php'); ?>
</body>

</html>